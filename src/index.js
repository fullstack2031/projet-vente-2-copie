
import ReactDOM from 'react-dom/client';
import {legacy_createStore as createStore} from 'redux'
import reducer from './redux/reducer';
import { Provider } from 'react-redux';
import App from './App';

const store=createStore(reducer)
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div>
<Provider store={store}>
<App/>
</Provider>
  </div>
);


